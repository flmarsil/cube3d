/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   close.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 12:44:48 by flmarsil          #+#    #+#             */
/*   Updated: 2020/03/11 12:08:40 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/cube3d.h"

void	free_tabmap(t_all *a)
{
	int y;

	y = 0;
	while (y < a->p.my)
		free(a->p.tabmap[y++]);
}

void	free_path(t_all *a)
{
	free(a->p.texture[0]);
	free(a->p.texture[1]);
	free(a->p.texture[2]);
	free(a->p.texture[3]);
	free(a->p.texture[4]);
}

int		ft_close(t_all *a, int ret_exit)
{
	free_path(a);
	free_tabmap(a);
	if (!a->m.window_p && !a->m.mlx_p)
		mlx_destroy_window(a->m.mlx_p, a->m.window_p);
	free(a->r.zbuffer);
	free(a->m.mlx_p);
	exit(ret_exit);
}
