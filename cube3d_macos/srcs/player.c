/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/09 17:00:23 by flmarsil          #+#    #+#             */
/*   Updated: 2020/03/11 12:03:49 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/cube3d.h"

void	fill_player(t_all *a, char *line, int y, int x)
{
	a->p.tabmap[y][x] = 0;
	if (!a->p.player[0])
	{
		a->p.player[0] = line[x];
		a->p.player[1] = y;
		a->p.player[2] = x;
	}
	else if (a->p.player[0] && ft_strchr("NSEW", line[x]))
		return (ft_error("Joueur : 1 maximum"));
}
