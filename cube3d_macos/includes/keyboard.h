/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keyboard.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/11 13:39:27 by flmarsil          #+#    #+#             */
/*   Updated: 2020/03/11 14:10:06 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYBOARD_H
# define KEYBOARD_H

# define KEY_ESC		53
# define KEY_W			13
# define KEY_A			0
# define KEY_D			2
# define KEY_S			1
# define ARROW_DOWN		125
# define ARROW_UP		126
# define ARROW_LEFT		123
# define ARROW_RIGHT	124

#endif
