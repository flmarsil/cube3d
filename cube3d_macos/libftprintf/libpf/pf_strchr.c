/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/12 13:54:00 by flmarsil          #+#    #+#             */
/*   Updated: 2020/02/05 12:21:15 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libpf.h"

char	*pf_strchr(const char *s, int c)
{
	while ((*s && *s != (char)c))
		s++;
	if (*s && *s == (char)c)
		return ((char *)s);
	return (NULL);
}
