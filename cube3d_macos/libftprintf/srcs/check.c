/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/06 19:55:49 by flmarsil          #+#    #+#             */
/*   Updated: 2020/02/05 12:22:26 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void	check_flag(t_elem *e, char const *s)
{
	while (s[e->i] == '-' || s[e->i] == '0' || s[e->i] == '+'
		|| s[e->i] == ' ' || s[e->i] == '#')
		e->flag = (s[(e->i)++] == '0' && e->flag != -1) ? 1 : -1;
}

void	check_width(va_list av, t_elem *e, char const *s)
{
	if (s[e->i] == '*')
	{
		e->width = va_arg(av, int);
		e->w_neg = (e->width < 0) ? 1 : 0;
		(e->i)++;
		return ;
	}
	while (pf_isdigit(s[(e->i)]) == 1)
		e->width = e->width * 10 + (s[(e->i)++] - '0');
}

void	check_precision(va_list av, t_elem *e, char const *s)
{
	if (s[e->i] == '.' && s[e->i + 1] == '*')
	{
		e->preci = va_arg(av, int);
		e->p_neg = (e->preci < 0) ? 1 : 0;
		(e->i) += 2;
	}
	else if (s[e->i] == '.')
	{
		(e->i)++;
		e->preci = 0;
		while (pf_isdigit(s[e->i]) == 1)
			e->preci = e->preci * 10 + (s[(e->i)++] - '0');
	}
}

int		isnull(va_list av, t_elem *e)
{
	char	*s_tmp;
	int		c_tmp;

	if (e->type == 's')
	{
		s_tmp = va_arg(av, char *);
		if (s_tmp == NULL && !(e->ret = pf_strdup("(null)")))
			return (-1);
		else if (s_tmp && !(e->ret = pf_strdup(s_tmp)))
			return (-1);
	}
	else
	{
		c_tmp = (va_arg(av, int));
		if ((c_tmp == 0 && e->width == 0))
			return (1);
		else if (!(e->ret = pf_strnew(1, c_tmp)))
			return (-1);
	}
	return (0);
}

int		check_type(va_list av, t_elem *e, char const *s)
{
	e->type = s[e->i];
	if (pf_strchr("spdiuxX%%", s[e->i]) == NULL)
		return (-1);
	if ((s[e->i] == 'd' || s[e->i] == 'i') &&
		!(e->ret = pf_itoa(va_arg(av, int))))
		return (-1);
	else if (s[e->i] == 'u' &&
		!(e->ret = pf_itoa_base(va_arg(av, unsigned int), "0123456789")))
		return (-1);
	else if ((s[e->i] == 'X' || s[e->i] == 'x' || s[e->i] == 'p') &&
		!(e->ret = convert_x(av, s[e->i], e)))
		return (-1);
	else if (s[e->i] == '%' && !(e->ret = pf_strnew(1, s[e->i])))
		return (-1);
	else if ((s[e->i] == 's') && isnull(av, e) == -1)
		return (-1);
	e->r_neg = (pf_atoi(e->ret) < 0 && e->type != 'u') ? 1 : 0;
	return (1);
}
