#ifndef KEYBOARD_H
# define KEYBOARD_H

# define PRESS 2
# define PRESS_MASK 1
# define RELEASE 3
# define RELEASE_MASK 2
# define KEY_ESC 65307
# define EXIT 33
# define EXIT_MASK 18
# define KEY_W 119
# define KEY_A 97
# define KEY_D 100
# define KEY_S 115
# define ARROW_LEFT	65361
# define ARROW_RIGHT 65363

#endif
