#include "../includes/libpf.h"

void	pf_putstr_fd(char *s, int fd)
{
	if (!s)
		return ;
	write(fd, s, pf_strlen(s));
}
