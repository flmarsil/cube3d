#include "../include/libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	unsigned	int	nb;
	char			c;

	nb = n;
	nb *= (n < 0) ? -1 : 1;
	if (n < 0)
		write(fd, "-", 1);
	if (nb > 9)
		ft_putnbr_fd(nb / 10, fd);
	c = (nb % 10) + '0';
	write(fd, &c, 1);
}
