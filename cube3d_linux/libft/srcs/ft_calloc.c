#include "../include/libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	void	*ret;

	if (!(ret = malloc(size * count)))
		return (NULL);
	if (!count || !size)
		return (ret);
	ft_bzero(ret, count * size);
	return (ret);
}
