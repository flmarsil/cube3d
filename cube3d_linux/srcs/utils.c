#include "../includes/cube3d.h"

void	init_struct(t_all *a)
{
	a->p.rx = 0;
	a->p.ry = 0;
	a->p.f = -1;
	a->p.c = -1;
	a->p.texture[0] = NULL;
	a->p.texture[1] = NULL;
	a->p.texture[2] = NULL;
	a->p.texture[3] = NULL;
	a->p.texture[4] = NULL;
	a->p.flag = 0;
	a->p.player[0] = 0;
	a->r.tex_nb = 0;
	a->r.camera_x = 0;
	a->r.map.x = 0;
	a->r.map.y = 0;
	a->r.tex_y = 0;
	a->r.tex_x = 0;
	a->r.move = 0;
	a->r.move_ad = 0;
	a->r.rot = 0;
	a->r.text_step = 0;
	a->r.text_pos = 0;
	a->r.sprites = 0;
	a->p.bloc = 0;
}

void	ft_error(char *error)
{
	ft_printf("\033[0;31m");
	ft_printf("\nERROR\n%s\n", error);
	ft_printf("\033[0m");
	exit(-1);
}

int		ft_iscomma(char c)
{
	return ((c == ',') ? 1 : 0);
}

int		comma_count(char *line, int x)
{
	int count;

	count = 0;
	while (line[x])
	{
		while (line[x] && (ft_isspace(line[x]) || ft_isdigit(line[x])))
			x++;
		while (line[x] && ft_iscomma(line[x]))
		{
			++count;
			x++;
		}
	}
	return (count);
}

int		numbers_count(char *line, int x, t_all *a)
{
	int ret;

	ret = 0;
	a->p.num_c = 0;
	while (line[x])
	{
		a->p.len_security = 0;
		while (line[x] && ft_isspace(line[x]))
			x++;
		if (line[x] && ft_isdigit(line[x]))
			++a->p.num_c;
		while (line[x] && ft_isdigit(line[x]))
		{
			x++;
			a->p.len_security++;
		}
		ret = len_security(a, line, x);
		if (ret == -1)
			return (-1);
		if (line[x] && ft_iscomma(line[x]))
			x++;
	}
	return (a->p.num_c);
}
